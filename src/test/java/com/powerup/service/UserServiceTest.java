package com.powerup.service;

import com.powerup.entity.User;
import com.powerup.exceptions.UserAlreadyExists;
import com.powerup.exceptions.UserNotFoundException;
import com.powerup.interfaces.IUserService;
import com.powerup.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {UserService.class})
class UserServiceTest {

    @Autowired
    private IUserService userService;

    @MockBean
    private UserRepository userRepository;

    @Test
    void createUserOkTest() {
        User mockUser = new User();
        mockUser.setName("New user");

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());
        when(userRepository.save(any(User.class))).thenReturn(mockUser);

        assertEquals("New user", userService.createUser(mockUser).getName());
    }

    @Test
    void userAlreadyExistsTest() {
        User mockUser = new User();
        mockUser.setEmail("test@mail.com");

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(mockUser));

        assertThrows(UserAlreadyExists.class, () -> userService.createUser(mockUser));
    }

    @Test
    void getUserByEmailOkTest() {
        User mockUser = new User();
        mockUser.setName("New user");

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(mockUser));

        assertEquals(mockUser, userService.getUserByEmail("test@email.com"));
    }

    @Test
    void userNotFoundTest() {

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> userService.getUserByEmail("test@email.com"));
    }

    @Test
    void getAllUsersOkTest() {
        List<User> allUsers = List.of(new User());

        when(userRepository.findAll()).thenReturn(allUsers);

        assertTrue(userService.getAllUsers().size() > 0);
    }
}
