package com.powerup.controller;

import com.powerup.entity.User;
import com.powerup.interfaces.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {UserController.class})
class UserControllerTest {

    @Autowired
    private UserController userController;

    @MockBean
    private IUserService userService;

    @Test
    void createUserOKTest() {
        User mockNewUser = new User();

        when(userService.createUser(any(User.class))).thenReturn(mockNewUser);

        assertEquals(HttpStatus.OK, userController.createUser(mockNewUser).getStatusCode());
    }

    @Test
    void getUserByEmailOkTest() {
        when(userService.getUserByEmail(anyString())).thenReturn(new User());

        assertEquals(HttpStatus.OK, userController.getUserByEmail("test@test.com").getStatusCode());
    }

    @Test
    void getAllUsersOkTest() {
        when(userService.getAllUsers()).thenReturn(List.of(new User()));

        assertEquals(HttpStatus.OK, userController.getAllUsers().getStatusCode());
    }
}
