package com.powerup.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.lang.reflect.Method;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UserExceptionHandler.class})
class UserExceptionHandlerTest {

    @Autowired
    private UserExceptionHandler userExceptionHandler;

    @Test
    public void handleValidationsExceptionTest() throws NoSuchMethodException {

        // mock MethodArgumentNotValidException
        BindingResult bindingResult = new MapBindingResult(new HashMap<>(), "objectName");
        bindingResult.addError(new FieldError("UserMockObject", "Name", "Name is required"));
        Method method = this.getClass().getMethod("handleValidationsExceptionTest", (Class<?>[]) null);
        MethodParameter parameter = new MethodParameter(method, -1);
        MethodArgumentNotValidException exception = new MethodArgumentNotValidException(parameter, bindingResult);

        assertEquals(HttpStatus.BAD_REQUEST, userExceptionHandler.handleValidationsException(exception).getStatusCode());
    }
}
