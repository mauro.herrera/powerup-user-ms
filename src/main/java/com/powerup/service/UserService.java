package com.powerup.service;

import com.powerup.entity.User;
import com.powerup.exceptions.UserAlreadyExists;
import com.powerup.exceptions.UserNotFoundException;
import com.powerup.interfaces.IUserService;
import com.powerup.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User newUser) {

        if (userRepository.findByEmail(newUser.getEmail()).isPresent()) {
            throw new UserAlreadyExists();
        }

        return userRepository.save(newUser);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
