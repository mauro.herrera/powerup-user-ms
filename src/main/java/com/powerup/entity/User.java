package com.powerup.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "powerup_user")
@Getter
@Setter
@ToString
public class User {

    @Schema(description = "Unique identifier of user", hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name is required")
    private String name;

    @NotBlank(message = "LastName is required")
    private String lastName;

    @Schema(example = "66678475")
    @NotBlank(message = "PhoneNumber is required")
    @Pattern(regexp = "^\\d*$", message = "Invalid phone number")
    private String phoneNumber;

    @NotBlank(message = "Address is required")
    private String address;

    @Schema(example = "openapi@test.com")
    @NotBlank(message = "Email is required")
    @Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "Invalid mail format")
    private String email;

    @Schema(example = "AbcDef123*")
    @NotBlank(message = "Password is required")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[*_-]).{8,15}$",
            message = "Password has to be between 8 and 15 characters and must have uppercase, lowercase, numbers and at least one character like '*_-'")
    private String password;
}
