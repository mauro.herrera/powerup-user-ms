package com.powerup.interfaces;

import com.powerup.entity.User;

import java.util.List;

public interface IUserService {

    User createUser(User newUser);
    User getUserByEmail(String email);
    List<User> getAllUsers();
}
