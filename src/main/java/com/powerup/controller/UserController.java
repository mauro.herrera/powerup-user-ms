package com.powerup.controller;

import com.powerup.entity.User;
import com.powerup.interfaces.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping
    public ResponseEntity<User> createUser(@Valid @RequestBody User newUser) {

        return new ResponseEntity<>(userService.createUser(newUser), HttpStatus.OK);
    }

    @Operation(summary = "Get user by email")
    @GetMapping(path = "/{email}")
    public ResponseEntity<User> getUserByEmail(@Parameter(description = "Email of user to be search") @PathVariable String email) {

        return new ResponseEntity<>(userService.getUserByEmail(email), HttpStatus.OK);
    }

    @Operation(summary = "Get all users")
    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {

        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }
}
