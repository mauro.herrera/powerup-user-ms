# User API

API user, first week challenge Power Up Java - AWS
Technologies:
 - Spring boot
 - Java 11
 - H2 Database
 - Lombok 
 - OpenAPI
 - JUnit


# Run project

On the project folder run ```mvn clean install``` and then ```java -jar target/user-ms-*.jar```.

# API Documentation

Once the project is up the documentation will be avalilable at local ```http://localhost:8080/swagger-ui/index.html```

# Unit test

For unit test run ```mvn clean verify``` and open the ***index.html*** file on ```target/site/jacoco/```

### Notes
 - Didn't create DTO cause would be the same as the entity, on future iterations as the complex of the project increase more cool stuff would be added
